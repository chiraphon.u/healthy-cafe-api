import React, { Component } from 'react'
import Layout from '../components/Layout';
import axios from 'axios';
import Link from 'next/link';



class index extends Component {
  static async getIntialProps() {
    const res = await axios.get("http://localhost:3001/blogs");
    return { blogs: res.data }
  }
  renderBlogs = blogs => {
    return (
      blogs.map(blogs => {
        return (
          <div key={blog.id} className="col-6">
            <img src={blog.thumbnail} className="img-fluid" />
            <h4 className="mt-3">
              <Link as={'/blog/detail/' + blog.id} href={'/blog/detail?id=' + blog.id}>
                {blog.subject}
              </Link>

            </h4>

          </div>
        )
      }
      )
    )
  }
  render() {
    return (
      <Layout>
        test
          </Layout>
    )
  }
}

export default index;