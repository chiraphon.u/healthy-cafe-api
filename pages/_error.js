import React, { Component } from 'react'
import Layout from '../components/Layout';


class _error extends Component {
  static getInitailProps({res,err}){
    const statusCode = res ? res.statusCode : err ? err.statusCode : null;
    return statusCode;

  }
  render() {
    return (
      <Layout>
         <div className="container">
           <div className="text-center">
            <h3>
              {this.props.statusCode ?
              'An error' + this.props.statusCode + ' occured on server' :
              'An error occured on client'
            
          }
            </h3>
           </div>
        
        </div>
      </Layout>
     
    )
  }
}

export default _error;
