import React, { Component } from 'react'
import Header from '../components/Header';
import Footer from '../components/Footer';
import Head from 'next/head';
import "../static/css/style.scss"

class Layout extends Component {
  render() {
    const {children,title ="เฮลตี้ คาเฟ่ บล๊อกเกอร์"} = this.props;
    return (
      <div>
        <Head>
          <title>{title}</title>
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" />
        <link rel="stylesheet" href="/_next/static/style.css"/>
        </Head>
        <Header/>
        {children}
        <Footer/>
      </div>
    )
  }
}
export default Layout;
